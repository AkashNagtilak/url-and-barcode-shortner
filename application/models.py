from django.db import models
from django.utils.translation import gettext_lazy as _
import qrcode
from PIL import Image, ImageDraw
from io import BytesIO
from django.core.files import File
import random


# Create your models here.
class URIShortnerModel(models.Model):
    """
        Model for url shortner.
    """
    full_url = models.URLField(_("Full URL"))
    short_url = models.URLField(_("Short URL"))
    qu_code = models.ImageField(_("QR Code"), upload_to= 'QR_Code/', blank= True, null= True)

    def __str__(self):
        return self.full_url

    class Meta:
        verbose_name = "URLShortner"
        verbose_name_plural = "URLShortner"


    def save(self,*args,**kwargs):
      qrcode_img=qrcode.make(self.full_url)
      canvas=Image.new("RGB", (400,400),"white")
      draw=ImageDraw.Draw(canvas)
      canvas.paste(qrcode_img)
      buffer=BytesIO()
      canvas.save(buffer,"PNG")
      self.qu_code.save(f'image{random.randint(0,9999)}.png',File(buffer),save=False)
      canvas.close()
      super().save(*args,**kwargs)
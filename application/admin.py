from dataclasses import field
from django.contrib import admin
from .models import URIShortnerModel

# Register your models here.
class URLAdmin(admin.ModelAdmin):
    """
        Admin for URL Admin.
    """
    model = URIShortnerModel
    field = "__all__"

admin.site.register(URIShortnerModel, URLAdmin)
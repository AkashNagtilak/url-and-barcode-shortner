from email import message
from importlib.resources import contents
from pyexpat import model
from pyexpat.errors import messages
from re import template
from django.shortcuts import render
from django.views.generic import View, ListView
from . import forms as forms
from .models import URIShortnerModel
from django.http import HttpResponseRedirect
import pyshorteners
import qrcode  

# Create your views here.
class URLShortnerView(View):
    """
        Class for URLShortner View.
    """
    model = URIShortnerModel
    template_name = 'application/url_shortner.html'
    form_class = forms.URLForm
    success_url = '/'

    def get(self, request, *args, **kwargs):

        context = {
            "form": self.form_class
        }
        return render(request, self.template_name, context= context)
    
    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            long_url = form.cleaned_data['full_url']
            type_tiny = pyshorteners.Shortener()
            short_url = type_tiny.tinyurl.short(long_url)
            self.model.objects.create(full_url= long_url, short_url= short_url)
            
            return HttpResponseRedirect('/url_shortner/url-list-view/')
        else:
            messages.error(request, 'Please enter valid url.')
        
        return render(request, self.template_name, {'form': form})


class URLListView(ListView):
    """
        Class for URL List View.
    """
    model = URIShortnerModel
    template_name = "application/url_list.html"
    queryset = URIShortnerModel.objects.all().order_by('-id')
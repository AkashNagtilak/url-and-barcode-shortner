from django import forms
from .models import URIShortnerModel

class URLForm(forms.Form):
    """
        Class form URL Shortner form.
    """
    full_url = forms.URLField()

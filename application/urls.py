from django.urls import path, include
from . import views as views

urlpatterns = [
    path("url-shortner-view/", views.URLShortnerView.as_view(), name="url-shortner-view"),
    path("url-list-view/", views.URLListView.as_view(), name= "url-list-view")
]
